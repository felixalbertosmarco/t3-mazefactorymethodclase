import {Room} from './Room.js';
import {Door} from './Door.js';
export class EnchantedDoor extends Door{
  constructor(r1:Room,r2:Room){
    super(r1,r2);
  }
}

