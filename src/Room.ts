import {Directions} from './Directions.js';
import {MapSite} from './MapSite.js';
export class Room extends MapSite{
  private roomNumber:number;
  private sides:MapSite[];
  constructor(n:number){
    super();
    this.sides=[];
    this.roomNumber=n;
  }
  public enter():void{
    console.log("Has entrado en la habitación",this.roomNumber);
  }
  public setSide(m:MapSite, d:Directions):void{
    this.sides[d]=m;
  }
  public getSide(d:Directions):MapSite{
    return this.sides[d];
  }
  public getRoomNumber():number{
    return this.roomNumber;
  }
}

