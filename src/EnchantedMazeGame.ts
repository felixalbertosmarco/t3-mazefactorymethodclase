import {Door} from './Door.js';
import {EnchantedDoor} from './EnchantedDoor.js';
import {Wall} from './Wall.js';
import {EnchantedWall} from './EnchantedWall.js';
import {EnchantedRoom} from './EnchantedRoom.js';
import {Room} from './Room.js';
import {MazeGame} from './MazeGame.js';
export class EnchantedMazeGame extends MazeGame{
  constructor(){
    super();
  }
  protected createRoom(n:number):Room{
    return new EnchantedRoom(n);
  }
  protected createWall():Wall{
    return new EnchantedWall();
  }
  protected createDoor(r1:Room,r2:Room):Door{
    return new EnchantedDoor(r1,r2);
  }
}

