import {Room} from './Room.js';
export class Maze{
  private rooms:Room[];
  constructor(){
    this.rooms=[];
  }
  public addRoom(r:Room):void{
    this.rooms[r.getRoomNumber()]=r;
  }
  public roomNo(n:number):Room{
    return this.rooms[n];
  }
}

