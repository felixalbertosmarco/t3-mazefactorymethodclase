import {EnchantedMazeGame} from './EnchantedMazeGame.js';
import {BombedMazeGame} from './BombedMazeGame.js';
import {Directions} from './Directions.js';
import {MazeGame} from './MazeGame.js';
import {Maze} from './Maze.js';
let mg:MazeGame = new EnchantedMazeGame();
let m:Maze = mg.createMaze();
