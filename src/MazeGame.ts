import {Directions} from './Directions.js';
import {Door} from './Door.js';
import {Maze} from './Maze.js';
import {Room} from './Room.js';
import {Wall} from './Wall.js';
export class MazeGame{
  constructor(){
    console.log("Tu nuevo mazegame !!!!");
  }
  public createMaze():Maze{
    let maze:Maze = new Maze();
    let r1:Room = this.createRoom(1);
    let r2:Room = this.createRoom(2);
    maze.addRoom(r1);
    maze.addRoom(r2);
    let door:Door = this.createDoor(r1,r2);

    r1.setSide(this.createWall(),Directions.North);
    r1.setSide(this.createWall(),Directions.South);
    r1.setSide(door,Directions.East);
    r1.setSide(this.createWall(),Directions.West);

    r2.setSide(this.createWall(),Directions.North);
    r2.setSide(this.createWall(),Directions.South);
    r2.setSide(this.createWall(),Directions.East);
    r2.setSide(door,Directions.West);

    console.log(door.otherSideFrom(r2));

    return maze;
  }
  protected createRoom(n:number):Room{
    return new Room(n);
  }
  protected createWall():Wall{
    return new Wall();
  }
  protected createDoor(r1:Room,r2:Room):Door{
    return new Door(r1,r2);
  }
}

