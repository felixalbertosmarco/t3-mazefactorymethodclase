import {Room} from './Room.js';
import {Wall} from './Wall.js';
import {Door} from './Door.js';
import {BombedDoor} from './BombedDoor.js';
import {BombedRoom} from './BombedRoom.js';
import {BombedWall} from './BombedWall.js';
import {MazeGame} from './MazeGame.js';
export class BombedMazeGame extends MazeGame{
  constructor(){
    super();
  }
  protected createRoom(n:number):Room{
    return new BombedRoom(n);
  }
  protected createWall():Wall{
    return new BombedWall();
  }
  protected createDoor(r1:Room,r2:Room):Door{
    return new BombedDoor(r1,r2);
  }
}

